package com.javaboy.vhr.service;

import com.github.pagehelper.PageInfo;
import com.javaboy.vhr.entity.CertFile;

import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-25 16:51:54
 * @description: 证书文件(CertFile)表服务接口
 */
public interface CertFileService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CertFile queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<CertFile> queryAllByLimit(int offset, int limit);

    /**
     * 分页查询
     *
     * @param certFile
     * @param pageNo
     * @param pageSize
     * @return
     */
    PageInfo<CertFile> pageList(CertFile certFile, Integer pageNo, Integer pageSize);

    /**
     * 新增数据
     *
     * @param certFile 实例对象
     * @return 实例对象
     */
    CertFile insert(CertFile certFile);

    /**
     * 修改数据
     *
     * @param certFile 实例对象
     * @return 实例对象
     */
    CertFile update(CertFile certFile);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}