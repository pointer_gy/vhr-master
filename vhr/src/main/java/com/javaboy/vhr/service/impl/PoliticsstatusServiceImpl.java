package com.javaboy.vhr.service.impl;

import com.javaboy.vhr.entity.Politicsstatus;
import com.javaboy.vhr.mapper.PoliticsstatusMapper;
import com.javaboy.vhr.service.PoliticsstatusService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-04 10:13:06
 * @description: 政治面貌(Politicsstatus)表服务实现类
 */
@Service("politicsstatusService")
public class PoliticsstatusServiceImpl implements PoliticsstatusService {
    @Resource
    private PoliticsstatusMapper politicsstatusMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Politicsstatus queryById(Integer id) {
        return this.politicsstatusMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Politicsstatus> queryAllByLimit(int offset, int limit) {
        return this.politicsstatusMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param politicsstatus 实例对象
     * @return 实例对象
     */
    @Override
    public Politicsstatus insert(Politicsstatus politicsstatus) {
        this.politicsstatusMapper.insert(politicsstatus);
        return politicsstatus;
    }

    /**
     * 修改数据
     *
     * @param politicsstatus 实例对象
     * @return 实例对象
     */
    @Override
    public Politicsstatus update(Politicsstatus politicsstatus) {
        this.politicsstatusMapper.update(politicsstatus);
        return this.queryById(politicsstatus.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.politicsstatusMapper.deleteById(id) > 0;
    }

    /**
     * 通过实体作为筛选条件查询
     * @param politicsstatus
     * @return
     */
    @Override
    public List<Politicsstatus> queryAll(Politicsstatus politicsstatus) {
        return this.politicsstatusMapper.queryAll(politicsstatus);
    }
}