package com.javaboy.vhr.service.impl;

import com.javaboy.vhr.mapper.MenuRoleMapper;
import com.javaboy.vhr.mapper.RoleMapper;
import com.javaboy.vhr.entity.Role;
import com.javaboy.vhr.service.RoleService;
import com.javaboy.vhr.utils.result.ResultDTO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Role)表服务实现类
 *
 * @author gaoyang
 * @since 2020-10-31 14:53:01
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleMapper roleDao;
    @Resource
    private MenuRoleMapper menuRoleDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Role queryById(Integer id) {
        return this.roleDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Role> queryAllByLimit(int offset, int limit) {
        return this.roleDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param role 实例对象
     * @return 实例对象
     */
    @Override
    public Role insert(Role role) {
        this.roleDao.insert(role);
        return role;
    }

    /**
     * 修改数据
     *
     * @param role 实例对象
     * @return 实例对象
     */
    @Override
    public Role update(Role role) {
        this.roleDao.update(role);
        return this.queryById(role.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public ResultDTO<Void> deleteById(Integer id) {
        if (this.menuRoleDao.queryByRid(id) == 1){
            return ResultDTO.error("请先取消角色对应的权限！");
        }
        this.roleDao.deleteById(id);
        return ResultDTO.success("删除成功");
    }

    @Override
    public List<Role> getAllRoles() {
        return this.roleDao.getAllRoles();
    }
}