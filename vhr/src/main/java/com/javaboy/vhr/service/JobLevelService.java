package com.javaboy.vhr.service;

import com.javaboy.vhr.entity.JobLevel;

import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-01-21 15:09:24
 * @description: 职称(JobLevel)表服务接口
 */
public interface JobLevelService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    JobLevel queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<JobLevel> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param jobLevel 实例对象
     * @return 实例对象
     */
    JobLevel insert(JobLevel jobLevel);

    /**
     * 修改数据
     *
     * @param jobLevel 实例对象
     * @return 实例对象
     */
    JobLevel update(JobLevel jobLevel);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 查询所有职称信息
     * @return
     */
    List<JobLevel> getAllJobLevels();

    /**
     * 批量删除
     * @param ids
     */
    void delJobLevesByIds(Integer[] ids);

    /**
     * 通过实体作为筛选条件查询
     * @param jobLevel
     * @return
     */
    List<JobLevel> queryAll(JobLevel jobLevel);
}