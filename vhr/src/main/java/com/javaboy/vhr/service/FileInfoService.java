package com.javaboy.vhr.service;

import com.github.pagehelper.PageInfo;
import com.javaboy.vhr.entity.FileInfo;
import com.javaboy.vhr.utils.result.ResultDTO;

import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-03-23 10:46:35
 * @description: 文件(FileInfo)表服务接口
 */
public interface FileInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    FileInfo queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<FileInfo> queryAllByLimit(int offset, int limit);

    /**
     * 分页查询
     *
     * @param fileInfo
     * @param pageNo
     * @param pageSize
     * @return
     */
    PageInfo<FileInfo> pageList(FileInfo fileInfo, Integer pageNo, Integer pageSize);

    /**
     * 新增数据
     *
     * @param fileInfo 实例对象
     * @return 实例对象
     */
    FileInfo insert(FileInfo fileInfo);

    /**
     * 修改数据
     *
     * @param fileInfo 实例对象
     * @return 实例对象
     */
    FileInfo update(FileInfo fileInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    /**
     * 根据key查询文件
     * @param fileKey
     * @return
     */
    FileInfo queryByKey(String fileKey);

}