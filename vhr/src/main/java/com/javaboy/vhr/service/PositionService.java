package com.javaboy.vhr.service;

import com.javaboy.vhr.entity.Position;

import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-01-09 14:55:59
 * @description: 职位表(Position)表服务接口
 */
public interface PositionService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Position queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Position> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param position 实例对象
     * @return 实例对象
     */
    Position insert(Position position);

    /**
     * 修改数据
     *
     * @param position 实例对象
     * @return 实例对象
     */
    Position update(Position position);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 查询所有职位信息
     * @return
     */
    List<Position> getAllPosition();

    /**
     * 批量删除职位
     * @param ids
     * @return
     */
    Integer deletePositionsByIds(Integer[] ids);

    /**
     * 通过实体作为筛选条件查询
     * @param position
     * @return
     */
    List<Position> queryAll(Position position);
}