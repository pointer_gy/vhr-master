package com.javaboy.vhr.service.impl;

import com.javaboy.vhr.entity.Position;
import com.javaboy.vhr.mapper.PositionMapper;
import com.javaboy.vhr.service.PositionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-01-09 14:55:59
 * @description: 职位表(Position)表服务实现类
 */
@Service("positionService")
public class PositionServiceImpl implements PositionService {
    @Resource
    private PositionMapper positionMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Position queryById(Integer id) {
        return this.positionMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Position> queryAllByLimit(int offset, int limit) {
        return this.positionMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param position 实例对象
     * @return 实例对象
     */
    @Override
    public Position insert(Position position) {
        position.setEnabled(true);
        position.setCreateDate(new Date());
        this.positionMapper.insert(position);
        return position;
    }

    /**
     * 修改数据
     *
     * @param position 实例对象
     * @return 实例对象
     */
    @Override
    public Position update(Position position) {
        this.positionMapper.update(position);
        return this.queryById(position.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.positionMapper.deleteById(id) > 0;
    }

    @Override
    public List<Position> getAllPosition() {
        return this.positionMapper.getAllPosition();
    }

    @Override
    public Integer deletePositionsByIds(Integer[] ids) {
        return this.positionMapper.deletePositionsByIds(ids);
    }

    /**
     * 通过实体作为筛选条件查询
     * @param position
     * @return
     */
    @Override
    public List<Position> queryAll(Position position) {
        return this.positionMapper.queryAll(position);
    }
}