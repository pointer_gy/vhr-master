package com.javaboy.vhr.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javaboy.vhr.entity.CertFile;
import com.javaboy.vhr.mapper.CertFileMapper;
import com.javaboy.vhr.service.CertFileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-25 16:51:54
 * @description: 证书文件(CertFile)表服务实现类
 */
@Service("certFileService")
public class CertFileServiceImpl implements CertFileService {
    @Resource
    private CertFileMapper certFileMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public CertFile queryById(Integer id) {
        return this.certFileMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<CertFile> queryAllByLimit(int offset, int limit) {
        return this.certFileMapper.queryAllByLimit(offset, limit);
    }

    @Override
    public PageInfo<CertFile> pageList(CertFile certFile, Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<CertFile> certFileList = this.certFileMapper.queryAll(certFile);
        PageInfo<CertFile> pageInfo = new PageInfo<>(certFileList);
        return pageInfo;
    }

    /**
     * 新增数据
     *
     * @param certFile 实例对象
     * @return 实例对象
     */
    @Override
    public CertFile insert(CertFile certFile) {
        this.certFileMapper.insert(certFile);
        return certFile;
    }

    /**
     * 修改数据
     *
     * @param certFile 实例对象
     * @return 实例对象
     */
    @Override
    public CertFile update(CertFile certFile) {
        this.certFileMapper.update(certFile);
        return this.queryById(certFile.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.certFileMapper.deleteById(id) > 0;
    }
}