package com.javaboy.vhr.service;

import com.javaboy.vhr.entity.Menu;

import java.util.List;

/**
 * (Menu)表服务接口
 *
 * @author gaoyang
 * @since 2020-10-31 14:52:42
 */
public interface MenuService {

    /**
     * 根据登录的 hrId 查询权限菜单
     * @return
     */
    List<Menu> getMenusByHrId();

    /**
     * 根据主键查询单条数据
     * @param id
     * @return
     */
    Menu queryById(Integer id);

    /**
     * 查询所有菜单
     * @return
     */
    List<Menu> queryAll();

    /**
     * 根据角色查询所有权限菜单
     * @return
     */
    List<Menu> getAllMenusWithRole();

    /**
     * 查询菜单列表
     * @return
     */
    List<Menu> getAllMenus();

    /**
     * 修改
     * @param rid
     * @param mids
     * @return
     */
    Boolean updateMenuRole(Integer rid, Integer[] mids);

    /**
     * 获取菜单id列表
     * @param rid
     * @return
     */
    List<Integer> getMidsByRid(Integer rid);
}