package com.javaboy.vhr.utils;

import com.javaboy.vhr.entity.Hr;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author: gaoyang
 * @date: 2021-02-24 18:18
 * @description:
 */
public class HrUtil {
    public static Hr getCurrentHr() {
        return ((Hr) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
