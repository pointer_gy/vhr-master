package com.javaboy.vhr.utils.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: gaoyang
 * @Date: 2020/10/24  15:30
 * @Description: 自定义返回结果
 */
@ApiModel("返回结果")
@Data
public class ResultDTO<T> implements Serializable {
    private static final long serialVersionUID = -686082072661156143L;

    @ApiModelProperty("返回状态")
    private Boolean status;
    @ApiModelProperty("返回信息")
    private String msg;
    @ApiModelProperty("业务数据")
    private T data;

    public static ResultDTO<Void> success() {
        final ResultDTO<Void> result = new ResultDTO<>();
        result.setStatus(true);
        result.setMsg("success");
        return result;
    }

    public static ResultDTO<Void> success(final String msg) {
        final ResultDTO<Void> result = new ResultDTO<>();
        result.setStatus(true);
        result.setMsg(msg);
        return result;
    }

    public static <T> ResultDTO<T> success(final T data) {
        final ResultDTO<T> result = new ResultDTO<>();
        result.setStatus(true);
        result.setMsg("success");
        result.setData(data);
        return result;
    }

    public static <T> ResultDTO<T> success(final String msg, final T data) {
        final ResultDTO<T> result = new ResultDTO<>();
        result.setStatus(true);
        result.setMsg(msg);
        result.setData(data);
        return result;
    }

    public static ResultDTO<Void> error() {
        final ResultDTO<Void> result = new ResultDTO<>();
        result.setStatus(false);
        result.setMsg("error");
        return result;
    }

    public static ResultDTO<Void> error(final String msg) {
        final ResultDTO<Void> result = new ResultDTO<>();
        result.setStatus(false);
        result.setMsg(msg);
        return result;
    }

    public static <T> ResultDTO<T> error(final String msg, final T data) {
        final ResultDTO<T> result = new ResultDTO<>();
        result.setStatus(false);
        result.setMsg(msg);
        result.setData(data);
        return result;
    }
}
