package com.javaboy.vhr.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: gaoyang
 * @date: 2020-12-29 10:42
 * @description: 测试接口
 */
@Slf4j
@Api(tags = "测试API")
@RestController
@RequestMapping("/hello")
public class HelloWorld {

    @Value("${localUploadFilePath}")
    private String localUploadFilePath;

    public static void main(String[] args) {
        String str = null;
        str = "1111";
        System.out.printf(str);
    }

}
