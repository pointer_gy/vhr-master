package com.javaboy.vhr.controller;

import com.javaboy.vhr.entity.Menu;
import com.javaboy.vhr.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: gaoyang
 * @Date: 2020/10/31  15:10
 * @Description:
 */
@Api(tags = "系统配置API")
@RestController
@RequestMapping("/system/config")
public class SystemConfigController {

    @Resource
    private MenuService menuService;

    /**
     * 根据登录的 hrId 查询权限菜单
     * 前端参数不可信，hr_id 由后端获取
     *
     * @return
     */
    @ApiOperation(value = "查询权限菜单")
    @GetMapping("/menu")
    public List<Menu> getMenusByHrId() {
        return menuService.getMenusByHrId();
    }

}
