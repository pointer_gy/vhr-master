package com.javaboy.vhr.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author: gaoyang
 * @date: 2020-12-24 13:11:48
 * @description: (HrRole)实体类
 */
@Getter
@Setter
public class HrRole implements Serializable {
    private static final long serialVersionUID = 885758579878896000L;

    private Integer id;

    private Integer hrid;

    private Integer rid;

}