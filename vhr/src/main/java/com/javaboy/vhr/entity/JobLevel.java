package com.javaboy.vhr.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: gaoyang
 * @date: 2021-01-21 15:09:22
 * @description: 职称(JobLevel)实体类
 */
@Getter
@Setter
@ApiModel("职称实体类")
public class JobLevel implements Serializable {
    private static final long serialVersionUID = -83270702350517361L;

    @ApiModelProperty("自增主键")
    private Integer id;

    @ApiModelProperty("职称名称")
    private String name;

    @ApiModelProperty("职称级别")
    private String titleLevel;

    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建日期")
    private Date createDate;

    @ApiModelProperty("是否启用")
    private Boolean enabled;

}