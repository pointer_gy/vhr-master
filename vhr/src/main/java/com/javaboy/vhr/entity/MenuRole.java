package com.javaboy.vhr.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author: gaoyang
 * @date: 2021-01-25 17:27:41
 * @description: (MenuRole)实体类
 */
@Getter
@Setter
@ApiModel("")
public class MenuRole implements Serializable {
    private static final long serialVersionUID = -90238978675269497L;

    @ApiModelProperty("自增主键")
    private Integer id;

    @ApiModelProperty("菜单id")
    private Integer mid;

    @ApiModelProperty("角色id")
    private Integer rid;

}