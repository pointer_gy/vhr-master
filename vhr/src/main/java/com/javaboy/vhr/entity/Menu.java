package com.javaboy.vhr.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2020-12-24 13:11:48
 * @description: 菜单实体类
 */
@ApiModel("菜单实体类")
@Getter
@Setter
public class Menu implements Serializable {
    private static final long serialVersionUID = -74884549012630901L;

    @ApiModelProperty("主键")
    private Integer id;

    @ApiModelProperty("权限控制使用")
    private String url;

    @ApiModelProperty("前端请求路径")
    private String path;

    @ApiModelProperty("组件名")
    private String component;

    @ApiModelProperty("组件名称")
    private String name;

    @ApiModelProperty("菜单图标")
    private String iconCls;

    @ApiModelProperty("是否保活：前端是否在内存中销毁")
    private Boolean keepAlive;

    @ApiModelProperty("是否需要认证")
    private Boolean requireAuth;

    @ApiModelProperty("父级id")
    private Integer parentId;

    @ApiModelProperty("是否启用")
    private Boolean enabled;

    private List<Menu> children;

    private List<Role> roles;

}