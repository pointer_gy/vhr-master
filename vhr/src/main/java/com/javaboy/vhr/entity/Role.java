package com.javaboy.vhr.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author: gaoyang
 * @date: 2020-12-24 13:11:48
 * @description: 角色(Role)实体类
 */

@Getter
@Setter
@ApiModel("角色实体类")
public class Role implements Serializable {
    private static final long serialVersionUID = -31851479187797041L;

    @ApiModelProperty("主键")
    private Integer id;

    @ApiModelProperty("角色英文名称")
    private String name;

    @ApiModelProperty("角色中文名称")
    private String nameZh;

}