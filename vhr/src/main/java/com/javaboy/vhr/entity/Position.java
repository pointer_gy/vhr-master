package com.javaboy.vhr.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: gaoyang
 * @date: 2021-01-09 14:55:56
 * @description: 职位表(Position)实体类
 */
@Getter
@Setter
@ApiModel("职位实体类")
public class Position implements Serializable {
    private static final long serialVersionUID = 688009648920340365L;

    @ApiModelProperty("自增主键")
    private Integer id;

    @ApiModelProperty("职位")
    private String name;

    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private Date createDate;

    @ApiModelProperty("是否启用")
    private Boolean enabled;

}