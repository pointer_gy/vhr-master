package com.javaboy.vhr;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @Author: gaoyang
 * @Date: 2020/10/24  14:47
 * @Description:
 */
@Slf4j
@SpringBootApplication
@MapperScan(basePackages = "com.javaboy.vhr.mapper")
public class VhrApplication {

    public static void main(String[] args) throws UnknownHostException {
        final ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(VhrApplication.class)
                .run(args);

        final Environment env = context.getEnvironment();
        VhrApplication.log.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! ActiveProfiles is '{}', Access URLs:\n\t" +
                        "swagger: \thttp://{}:{}/vhr/swagger-ui/index.html\n\t" +
                        "Local: \t\thttp://127.0.0.1:{}\n\t" +
                        "External: \thttp://{}:{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("spring.profiles.active"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"));
    }

}
