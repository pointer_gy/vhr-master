package com.javaboy.vhr.mapper;

import com.javaboy.vhr.entity.Menu;

import java.util.List;

/**
 * (Menu)表数据库访问层
 *
 * @author gaoyang
 * @since 2020-10-31 16:26:24
 */
public interface MenuMapper {
    /**
     * 根据登录的 hrId 查询权限菜单
     * @param hrId
     * @return
     */
    List<Menu> getMenusByHrId(Integer hrId);

    /**
     * 根据主键查询单条数据
     * @param id
     * @return
     */
    Menu queryById(Integer id);

    /**
     * 查询所有菜单
     * @param menu
     * @return
     */
    List<Menu> queryAll(Menu menu);

    /**
     * 根据角色查询所有权限菜单
     * @return
     */
    List<Menu> getAllMenusWithRole();

    /**
     * 查询菜单列表
     * @return
     */
    List<Menu> getAllMenus();

}