package com.javaboy.vhr.mapper;

import com.javaboy.vhr.entity.FileInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-03-23 10:46:37
 * @description: 文件(FileInfo)表数据库访问层
 */
public interface FileInfoMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    FileInfo queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<FileInfo> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param fileInfo 实例对象
     * @return 对象列表
     */
    List<FileInfo> queryAll(FileInfo fileInfo);

    /**
     * 新增数据
     *
     * @param fileInfo 实例对象
     * @return 影响行数
     */
    int insert(FileInfo fileInfo);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<FileInfo> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<FileInfo> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<FileInfo> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<FileInfo> entities);

    /**
     * 修改数据
     *
     * @param fileInfo 实例对象
     * @return 影响行数
     */
    int update(FileInfo fileInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    FileInfo queryByKey(String fileKey);
}