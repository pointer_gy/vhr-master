let proxyObj = {};

proxyObj['/vhr'] = {
    ws: false,
    target: 'http://localhost:8090', //代理转发地址
    changeOrigin: true,
    pathRewrite: {
        '^/': ''
    }
};

module.exports = {
    devServer: {
        host: 'localhost',
        port: 8082,
        proxy: proxyObj
    },
    //修改或新增html-webpack-plugin的值，在index.html里面能读取htmlWebpackPlugin.options.title
    chainWebpack: config => {
        config.plugin('html')
            .tap(args => {
                args[0].title = "人事管理系统";
                return args;
            })
    }
};